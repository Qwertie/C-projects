#include "1.h"
#include <stdio.h>
#include <assert.h>

bool check(int ints[3], int result[3])
{
	bool same = true;
	for (int i = 0; i < 3; i++)
	{
		if (ints[i] != result[i])
		{
			same = false;
		}
	}
	return same;
}

int main()
{
	int ints[3] = {32,5,54};
	int result[3] = {5,32,54};
	sort(ints);

	for (int i = 0; i < 3; i++)
	{
		printf("%d ", ints[i]);
	}
	printf("\n");


	assert(check(ints, result));

	int ints1[3] = {-5,10,0};
	int result1[3] = {-5,0,10};
	sort(ints1);
	assert(check(ints1, result1));

	for (int i = 0; i < 3; i++)
	{
		printf("%d ", ints1[i]);
	}
	printf("\n");

	printf("All tests passed\n");

}
