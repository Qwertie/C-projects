#include <stdio.h>
#include <stdlib.h>

int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

int main()
{
	int size;
	scanf ("%d", &size);

	int* nums = (int*)malloc(sizeof(int) * size);

	for (int i = 0; i < size; i++)
	{
		scanf ("%d", &nums[i]);
	}

	qsort (nums, size, sizeof(int), compare);

	printf("%d", nums[0]);
	printf("%c", ' ');
	printf("%d", nums[size - 1]);
	printf("\n");

    return 0;
}
