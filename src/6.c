#include <stdio.h>
#include <stdlib.h>

int main()
{
	char input[2][100];

	// Reset arrays to 0
	for (int i = 0; i < 100; i++)
	{
		input[0][i] = 0;
		input[1][i] = 0;
	}

	int length[2];

	scanf ("%s%n", &input[0][0], &length[0]);
	scanf (" "); // Catch the newline
	scanf ("%s%n", &input[1][0], &length[1]);



	// Format user input
	for (int i = 0; i < 2; i++)
	{
		// Convert chars to ints
		for (int b = 0; b < length[i]; b++)
		{
			input[i][b] = input[i][b] - '0';
		}
		// Shift input to end of array and fill start with 0
		for (int a = length[i]; a > 0; a--)
		{
			input[i][(99 - length[i]) + a] = input[i][a - 1];
			input[i][a-1] = 0;
		}
	}

	int longest = length[0] > length[1] ? length[0] : length[1];
	char* output = (char*)malloc(sizeof(char) * longest + 1);

	int carry = 0;
	for (int i = 99; i >= (100 - longest); i--)
	{
		int result = input[0][i] + input[1][i] + carry;
		int remainder = result % 10;
		carry = result / 10;
		output[longest - (99 - i)] = remainder;
	}
	output[0] = carry;


	// Don't output a 0 at the start of output
	int output_offset = 0;
	if (output[0] == 0)
		output_offset = 1;


	for (int i = output_offset; i < longest + 1; i++)
		printf("%d",output[i]);

	printf("\n");
	return 0;
}
